




Highcharts.chart('container', {
  chart: {
    type: chartData.chartType
  },
  title: {
    text: chartData.titleText
  },
  subtitle: {
    text: chartData.subtitleText
  },
  xAxis: {
    categories: chartData.categories,
    title: {
      text: chartData.xAxisTitle
    }
  },
  yAxis: {
    min: 0,
    title: {
      text: chartData.yAxisText,
      align: 'high'
    },
    labels: {
      overflow: 'justify'
    }
  },
  tooltip: {
    valueSuffix: chartData.tooltipValueSuffix
  },
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true
      }
    }
  },
  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    x: -0,
    y: -5,
    floating: true,
    borderWidth: 1,
    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    shadow: true
  },
  credits: {
    enabled: false
  },
  series: chartData.seriesData

});

