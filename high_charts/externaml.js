Highcharts.chart('container', {
  chart: {
    type: chartData.chartType
  },
  title: {
    text: chartData.titleText
  },
  subtitle: {
    text: chartData.subtitleText
  },
  xAxis: {
    categories: chartData.xAxisCategories,
    title: {
      text: chartData.xAxisTitleText
    }
  },
  yAxis: {
    min: 0,
    title: {
      text: chartData.yAxisTitleText,
      align: 'high'
    },
    labels: {
      overflow: 'justify'
    }
  },
  tooltip: {
    valueSuffix: chartData.tooltipValueSuffix
  },
  plotOptions: {
    bar: {
      dataLabels: {
        enabled: true
      }
    },
    series: {
      stacking: 'normal'
    }
  },

  legend: {
    layout: 'vertical',
    align: 'right',
    verticalAlign: 'top',
    x: 10,
    y: -5,
    floating: true,
    borderWidth: 1,
    backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
    shadow: true
  },
  credits: {
    enabled: false
  },
  series: chartData.series

});