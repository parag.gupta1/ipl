
const fileReader = require('./fileReader.js')

async function getMatchIds(year){
    
    const matchIds=[];
    function calculate(match, matchIds){
        const id = 0;
        const season = 1;
        if (match[season] == year) {
            matchIds.push(match[id]);
        }
    
    }
   await fileReader.rowReader('Project-IPL-Dataset/matches.csv', calculate, matchIds) 
    // console.log(matchIds);
    return matchIds
}

// getMatchIds(2015);
module.exports={getMatchIds};