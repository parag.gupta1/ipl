In this data assignment you will transform raw data of IPL to calculate the following stats:
1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015

2016 who have scored how much 6 

Implement the 4 functions, one for each task.
Write unit tests for all 4 functions
Use the functions results to dump json files

Checklist 
0. Proper naming conventions
1. Unit tests
2. Eslint
3. Error handling (try | catch)
4. Using Iterator| Map| Reduce| Filter
5. Creating 4 git branches for different stats.

`load data local infile './Project-IPL-Dataset/deliveries.csv' into table deliveries fields terminated by 
    "," enclosed by '"' lines terminated by "\n"  ignore 1 lines`

select c1 year,count(*) played_matches from matches group by c1
select c1 year,c10 team,count(*) won_matches from matches group by c1,c10
select b.c3 team,sum(b.c16) extra_runs from matches a inner join deliveries b on a.c0=b.c0 where a.c1="2016"  group by b.c3
select bowler,sum(total_runs),count(*) total_ball,sum(extra_ball) extra_ball,(count(*)-sum(extra_ball)) actual_ball,(sum(total_runs)/(count(*)-sum(extra_ball)) ) economy from (select b.c8 bowler,b.c17 total_runs,b.c16 extra_run,if(b.c16,1,0) extra_ball from matches a inner join deliveries b on a.c0=b.c0 where a.c1="2015" ) temp group by bowler order by economy limit 10