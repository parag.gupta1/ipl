//year_mathch={}
//extract data rows
//each row find team,year
//year_mathch me year na ho to bnao year=1; nhi to year++


const csv = require('fast-csv');
const fs = require('fs');

(() => {
    let header=1;
    let matchesWonPerTeamPerSeason = {};
    fs.createReadStream('Project-IPL-Dataset/matches.csv')
        .pipe(csv.parse())
        .on('error', error => console.error(error))
        .on('data', match => {
            if(!header){
            manipulation(match, matchesWonPerTeamPerSeason);
            }else{
                header=0;
            }
        })
        .on('end', matchCount => {
            console.log(`Parsed ${matchCount} matchs`);
            console.log(matchesWonPerTeamPerSeason);
            fs.writeFile('./jsonData/task2.json',JSON.stringify(matchesWonPerTeamPerSeason),"utf8",(err)=>{if(err) console.log(err);console.log("data saved in ./jsonData/task2.json")})
        });
})()

const manipulation = (match, matchesWonPerTeamPerSeason) => {
    const team = 10;
    const year = 1;
    const allYear=['2008', '2009', '2010', '2011', '2012','2013', '2014', '2015', '2016', '2017']
    if (match[team] in matchesWonPerTeamPerSeason) {
        if (match[year] in matchesWonPerTeamPerSeason[match[team]]) {
            matchesWonPerTeamPerSeason[match[team]][match[year]]++;
        } else {
            matchesWonPerTeamPerSeason[match[team]][match[year]] = 1;
        }
    } else {
        matchesWonPerTeamPerSeason[match[team]] = new Object();
        allYear.forEach(year => {
            matchesWonPerTeamPerSeason[match[team]][year]=0;
        });
        matchesWonPerTeamPerSeason[match[team]][match[year]] = 1;
    }
}