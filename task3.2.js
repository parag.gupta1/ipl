//year_mathch={}
//extract data rows
//each row find year 2016
//year_mathch me year na ho to bnao year=1; nhi to year++


const csv = require('fast-csv');
const fs = require('fs');
const helper = require('./helper.js');

(() => {

    let extraRunsConcededPerTeam2016 = {};
    const matchIds = new Array();
    fs.createReadStream('Project-IPL-Dataset/matches.csv')
        .pipe(csv.parse())
        .on('error', error => console.error(error))
        .on('data', match => {
            helper.matchIdsPerSeason(match, "2016", matchIds);
        })
        .on('end', matchCount => {
            console.log(`Parsed ${matchCount} rows`);
            fs.createReadStream('Project-IPL-Dataset/deliveries.csv')
                .pipe(csv.parse())
                .on('error', error => console.error(error))
                .on('data', deliveries => {
                    getRunsConcededIn2016(deliveries, matchIds, extraRunsConcededPerTeam2016);
                })
                .on('end', matchCount => {
                    console.log(`Parsed ${matchCount} rows`);
                    console.log(extraRunsConcededPerTeam2016);
                })
        });
})();

function getRunsConcededIn2016(deliveries, matchIds, extraRunsConcededPerTeam2016) {
    const team = 10;
    const year = 1;
    const id = 0;
    const bowlingTeam = 3;
    const extraRuns = 16;
    if (matchIds.find(matchId => matchId == deliveries[id])) {
        if (deliveries[bowlingTeam] in extraRunsConcededPerTeam2016) {
            extraRunsConcededPerTeam2016[deliveries[bowlingTeam]] += +deliveries[extraRuns]
        } else {
            extraRunsConcededPerTeam2016[deliveries[bowlingTeam]] = +deliveries[extraRuns]
        }

    }

}