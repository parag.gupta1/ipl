const fileReader = require('./fileReader.js');
const iplHelper = require('./iplHelper.js');

async function execute() {
    let strikeratePerBatesMan = {};
    let matchIds = await iplHelper.getMatchIds("2013");
    console.log(matchIds.length)
    await fileReader.rowReader('Project-IPL-Dataset/deliveries.csv', extract, strikeratePerBatesMan, matchIds);
    console.log(strikeratePerBatesMan);
    console.log(transform(strikeratePerBatesMan));
}
execute();

const extract = (delivery, strikeratePerBatesMan, matchIds) => {
    //    const total_runs = delivery[17];
    const id = delivery[0];
    const batsman = delivery[6];
    const wideRuns = delivery[10];
    const batsmanRan = delivery[15];
    const isSuperOver = delivery[9];

    if (matchIds.find(matchId => matchId == id) && +wideRuns == 0 && isSuperOver == 0) {
        if (batsman in strikeratePerBatesMan) {
            strikeratePerBatesMan[batsman]['totalBall'] += 1;
            strikeratePerBatesMan[batsman]['totalRun'] += +batsmanRan;
        } else {
            strikeratePerBatesMan[batsman] = {};
            strikeratePerBatesMan[batsman]['totalRun'] = +batsmanRan;
            strikeratePerBatesMan[batsman]['totalBall'] = 1;
        }
    }
}

function transform(strikeratePerBatesMan) {
    for (const batesMan in strikeratePerBatesMan) {
        if (strikeratePerBatesMan[batesMan]['totalRun'] > 200)
            strikeratePerBatesMan[batesMan] = Math.round(10000 * strikeratePerBatesMan[batesMan]['totalRun'] / strikeratePerBatesMan[batesMan]['totalBall']) / 100;
        else
            delete strikeratePerBatesMan[batesMan];
    }
    strikeratePerBatesMan = Object.entries(strikeratePerBatesMan)
    strikeratePerBatesMan = strikeratePerBatesMan.sort((a, b) => a[1] - b[1])
    return strikeratePerBatesMan;
}