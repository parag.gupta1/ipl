const fileReader = require('./fileReader.js');
const iplHelper = require('./iplHelper.js');


async function getResult() {

   let sixesPerBatesman = {};
   let matchIds = await iplHelper.getMatchIds("2015");
   console.log(matchIds.length)

   await fileReader.rowReader('Project-IPL-Dataset/deliveries.csv', getSixesPerBatesMan, sixesPerBatesman, matchIds);
   console.log(sixesPerBatesman);
}

getResult();

const getSixesPerBatesMan = (delivery, sixesPerBatesman, matchIds) => {
   const total_runs = 17;
   const id = 0;
   const batsman = 6;
   if (matchIds.find(eid => eid == delivery[id]) && delivery[total_runs] == 6) {
      if (delivery[batsman] in sixesPerBatesman) {
         sixesPerBatesman[delivery[batsman]] += 1
      } else {
         sixesPerBatesman[delivery[batsman]] = 1;
      }

   }
}