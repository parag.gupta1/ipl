const {
  Match,
  sequelize
} = require("./Model/match");
const {
  Deliveries
} = require("./Model/delivery");

function getEconomy() {
  Deliveries.findAll({
      // limit: 10,
      attributes: ['bowler',

        [sequelize.fn('sum', sequelize.col('total_runs')), 'total_runs'],
        [sequelize.fn('sum', sequelize.col('bye_runs')), 'bye_runs'],
        [sequelize.fn('sum', sequelize.col('legbye_runs')), 'legbye_runs'],
        [sequelize.fn('count', sequelize.col('ball')), 'ball']

      ],
      raw: true,
      include: [{
        model: Match,
        where: {
          season: '2015'
        },
        attributes: []
        // required: false,
        // duplicating: false // prevents sub-queries

      }],

      group: ['bowler']
    })
    .then((bowlerRunAndBall) => {
      let economy = {};
      bowlerRunAndBall.forEach(bowler => {
        economy[bowler['bowler']] = (bowler['total_runs'] - bowler['legbye_runs'] - bowler['bye_runs']) * 6 / bowler['ball']
      });
      // console.log(economy);
      let sortedEconomy = Object.entries(economy).sort((a, b) => a[1] - b[1])
      sortedEconomy=sortedEconomy.slice(0, 10);
      console.log(sortedEconomy);
      let sortedEconomyJson={};
      sortedEconomy.forEach((economy)=>{sortedEconomyJson[economy[0]]=economy[1]})
      console.log(sortedEconomyJson)
    });
}

getEconomy();