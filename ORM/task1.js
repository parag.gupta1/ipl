const {
  Match,
  sequelize
} = require("./Model/match");

function getTeamPlayedMatch() {
  Match.findAll({
    attributes: ['season', [sequelize.fn('count', sequelize.col('id')), 'played_matches']],
    group: ['season'],
    raw: true
  }).then((data) => {
    console.log(data)
  })
}

getTeamPlayedMatch();