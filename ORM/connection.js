const Sequelize = require('sequelize')
const mysql2 = require('mysql2')

const sequelize = new Sequelize('ipl', 'root', '', {
  dialect: 'mysql',
  define: {
    timestamps: false
  }
})

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

module.exports = sequelize