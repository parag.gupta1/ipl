// const sequelize = require('../connection.js')
const Sequelize = require('sequelize')
const {
  Match,
  sequelize
} = require("./match");


const Deliveries = sequelize.define('deliveries2', {
  match_id: {
    type: Sequelize.INTEGER,

    references: {
      // This is a reference to another model
      model: Match,

      // This is the column name of the referenced model
      key: 'id',
    }
  },
  inning: Sequelize.STRING,
  batting_team: Sequelize.STRING,
  bowling_team: Sequelize.STRING,
  over: Sequelize.INTEGER,
  ball: Sequelize.INTEGER,
  batsman: Sequelize.STRING,
  non_striker: Sequelize.STRING,
  bowler: Sequelize.STRING,
  is_super_over: Sequelize.INTEGER,
  wide_runs: Sequelize.INTEGER,
  bye_runs: Sequelize.INTEGER,
  legbye_runs: Sequelize.INTEGER,
  noball_runs: Sequelize.INTEGER,
  penalty_runs: Sequelize.INTEGER,
  batsman_runs: Sequelize.INTEGER,
  extra_runs: Sequelize.INTEGER,
  total_runs: Sequelize.INTEGER
  //  player_dismissed:Sequelize.STRING,
  //  dismissal_kind:Sequelize.STRING,
  //  fielder:Sequelize.STRING
})
Deliveries.belongsTo(Match, {
  foreignKey: 'match_id'
})

//This adds match.id attribute to deliveries as match_id
// Match.hasMany(Deliveries, {foreignKey: 'id'})  // This adds match.id attribute to deliveries


// Deliveries.sync().then((x) => {
//     console.log('New table created'+x);
// }).finally(() => {
//     sequelize.close();
// })

module.exports = {
  Deliveries,
  sequelize
}