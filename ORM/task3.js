const {
  Match,
  sequelize
} = require("./Model/match.js");
const {
  Deliveries
} = require("./Model/delivery.js");

function getExtraRunConceded() {
  Deliveries.findAll({
      attributes: ['bowling_team', [sequelize.fn('sum', sequelize.col('extra_runs')), 'extra_runs']],
      raw: true,
      include: [{
        model: Match,
        where: {
          season: '2016'
        },
        attributes: []
      }],

      group: ['bowling_team']
    })
    .then((data) => {
      console.log(data)
    });
}

getExtraRunConceded();