const {
  Match,
  sequelize
} = require("./Model/match");

function getSeasonWinnerTeam() {
  Match.findAll({
    attributes: ['season', 'winner', [sequelize.fn('count', sequelize.col('id')), 'played_matches']],
    group: ['season', 'winner'],
    raw: true
  }).then((data) => {
    console.log(data)
  })
}

getSeasonWinnerTeam();