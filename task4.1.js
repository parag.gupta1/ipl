const csv = require('fast-csv');
const fs = require('fs')
const helper = require('./helper.js');

(() => {
    let economyRatesPerBowler2015 = {};
    const matchIds = new Array();
    fs.createReadStream('Project-IPL-Dataset/matches.csv')
        .pipe(csv.parse())
        .on('error', error => console.error(error))
        .on('data', match => {
            helper.matchIdsPerSeason(match, "2015", matchIds);
        })
        .on('end', matchCount => {

            console.log(`Parsed ${matchCount} matchs`);
            fs.createReadStream('Project-IPL-Dataset/deliveries.csv')
                .pipe(csv.parse())
                .on('error', error => console.error(error))
                .on('data', delivery => {
                    getBowlerData(delivery, matchIds, economyRatesPerBowler2015);
                })
                .on('end', matchCount => {
                    console.log(`Parsed ${matchCount} matchs `);
                    // console.log(economyRatesPerBowler2015);
                    calculateEconomyRates(economyRatesPerBowler2015);
                })


        });
})()

function getBowlerData(delivery, matchIds, economyRatesPerBowler2015) {
    const bowler = 8;
    const year = 1;
    const id = 0;
    const matchId = 0;
    const over = 4;
    const totalRuns = 17;

    if (matchIds.find(eid => eid == delivery[id])) {
        if (delivery[bowler] in economyRatesPerBowler2015) {
            economyRatesPerBowler2015[delivery[bowler]]['totalRuns'] += +delivery[totalRuns]
            if (economyRatesPerBowler2015[delivery[bowler]]['last_over'] != delivery[over] || economyRatesPerBowler2015[delivery[bowler]]['last_match'] != delivery[matchId]) {
                economyRatesPerBowler2015[delivery[bowler]]['total_over'] += 1
                economyRatesPerBowler2015[delivery[bowler]]['last_over'] = delivery[over]
                economyRatesPerBowler2015[delivery[bowler]]['last_match'] = delivery[matchId]
            }
        } else {
            // economyRatesPerBowler2015[delivery[bowler]]= +delivery[totalRuns]
            economyRatesPerBowler2015[delivery[bowler]] = {}
            economyRatesPerBowler2015[delivery[bowler]]['totalRuns'] = +delivery[totalRuns]
            economyRatesPerBowler2015[delivery[bowler]]['total_over'] = 1
            economyRatesPerBowler2015[delivery[bowler]]['last_over'] = +delivery[over]
            economyRatesPerBowler2015[delivery[bowler]]['last_match'] = +delivery[matchId]
        }

    }
}

const calculateEconomyRates = (economyRatesPerBowler2015) => {
    let BowlerRunOver = Object.entries(economyRatesPerBowler2015)
    BowlerRunOver = BowlerRunOver.sort((a, b) => a[1]["totalRuns"] / a[1]["total_over"] - b[1]["totalRuns"] / b[1]["total_over"])
    // console.log(BowlerRunOver);
    for (let i = 0; i < 10; i++) {
        console.log(BowlerRunOver[i][0]);
    }
}