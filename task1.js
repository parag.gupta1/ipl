//year_mathch={}
//extract data rows
//each row find year
//year_mathch me year na ho to bnao year=1; nhi to year++
const csv = require('fast-csv');
const fs = require('fs');
(() => {
    let matchesPerSeason = {};
    let header = 1;
    fs.createReadStream('Project-IPL-Dataset/matches.csv')
        .pipe(csv.parse({
            headers: false
        })) //gives array as obj
        // .pipe(csv.parse({ headers : true })) gives row as obj 
        .on('error', error => console.error(error))
        .on('data', match => {
            // console.log(`ROW=${JSON.stringify(match)}`)
            if (!header) {
                let season = match[1];
                if (season in matchesPerSeason) {
                    matchesPerSeason[season]++;
                } else {
                    matchesPerSeason[season] = 1;
                }
            } else {
                header = 0;
            }
        })
        .on('end', matchCount => {
            console.log(`Parsed ${matchCount} rows`);
            console.log(matchesPerSeason)
            fs.writeFile('./jsonData/task1.json',JSON.stringify([matchesPerSeason]),"utf8",(err)=>{if(err) console.log(err);console.log("data saved in ./jsonData/task1.json")})
        });
})()



// https://stackoverflow.com/questions/49805899/reading-csv-in-nodejs-and-creating-new-file-for-each-line
// https://github.com/C2FO/fast-csv/blob/HEAD/docs/parsing.md#csv-parse
// https://stackoverflow.com/questions/49713174/nodejs-convert-text-to-json/49713282#49713282