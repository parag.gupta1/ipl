const csv = require('fast-csv');
const fs = require('fs');

module.exports = {
    rowReader: function (fileName, callbackFunction, result ,anotherArg) {
        return new Promise((resolve) => {
            fs.createReadStream(fileName)
                .pipe(csv.parse())
                .on('error', error => console.error(error))
                .on('data', row => {
                    callbackFunction(row, result ,anotherArg);
                })
                .on('end', matchCount => {
                    resolve(result)
                })
        })

    }
}