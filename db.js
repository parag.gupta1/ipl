const mysql=require('promise-mysql');

const connectionOptions={
    host:"localhost",
    database:"ipl",
    user:"root",
    password:"",
    connectionLimit : 10
}

async function getConnection(){

    
    pool=await mysql.createPool(connectionOptions);
    pool.on('connection',()=>{console.log('DB Connection established');})
    pool.on('error',(err)=>{console.error(err);})
    pool.on('acquire',(connection)=>{console.log('new connection %d acquired', connection.threadId)})
    pool.on('enqueue',()=>{console.log('waiting for new connection')})
    pool.on('release',(connection)=>{console.log('connection %d released', connection.threadId)})
    let sql=`load data local infile './Project-IPL-Dataset/deliveries.csv' into table deliveries2s fields terminated by 
    "," enclosed by '"' lines terminated by "\n"  ignore 1 lines (id,match_id,inning,batting_team,bowling_team,over,ball,batsman,non_striker,bowler,is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs,penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed,dismissal_kind,fielder
)`
    pool.query(sql)
    .then((sql_result)=>{
        // console.log(sql_result)// array of row object
        console.log("**************** DATABASE CONNECTED ******************")  
        return pool;     
    })
    .catch((err)=>{console.error("There is error in connection "+err)});
}

module.exports=getConnection();

