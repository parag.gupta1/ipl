const csv = require('fast-csv');
const fs = require('fs');

function rowReader(fileName, callbackFunction, result) {
    return new Promise((resolve) => {
        fs.createReadStream(fileName)
            .pipe(csv.parse())
            .on('error', error => console.error(error))
            .on('data', row => {
                callbackFunction(row, result);
            })
            .on('end', matchCount => {
                resolve(result)
            })
    })

}