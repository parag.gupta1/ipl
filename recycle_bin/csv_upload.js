const mysql=require('promise-mysql');

const connectionOptions={
    host:"localhost",
    database:"ipl",
    user:"root",
    password:"",
    connectionLimit : 10
}

async function getConnection(){

    
    pool=await mysql.createPool(connectionOptions);
    pool.on('connection',()=>{console.log('DB Connection established');})
    pool.on('error',(err)=>{console.error(err);})
    pool.on('acquire',(connection)=>{console.log('new connection %d acquired', connection.threadId)})
    pool.on('enqueue',()=>{console.log('waiting for new connection')})
    pool.on('release',(connection)=>{console.log('connection %d released', connection.threadId)})

    pool.query("select 1")
    .then((sql_result)=>{
        // console.log(sql_result)// array of row object
        console.log("**************** DATABASE CONNECTED ******************")  
        return pool;     
    })
    .catch((err)=>{console.error("There is error in connection "+err)});
}

module.exports=getConnection();

